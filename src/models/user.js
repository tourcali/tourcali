export default class User {
  constructor(nombre, apellido, correo, user, password) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.correo = correo;
    this.user = user;    
    this.password = password;
  }
}
