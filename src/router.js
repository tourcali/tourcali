import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';
import Menu from './views/Menu.vue';
import Email from './views/Email.vue';
import Inactive from './views/Inactive.vue';


Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/register',
      component: Register
    },
    {
      path: '/menu',
      component: Menu
    },
    {
      path: '/email',
      component: Email
    },
    {
      path: '/inactive',
      component: Inactive
    },
    {
      path: '/menu',
      name: 'menu',
      // lazy-loaded
      component: () => import('./views/Menu.vue')
    },
    {
      path: '/email',
      name: 'email',
      // lazy-loaded
      component: () => import('./views/Email.vue')
    },
    {
      path: '/inactive',
      name: 'inactive',
      // lazy-loaded
      component: () => import('./views/Inactive.vue')
    },
    {
      path: '/admin',
      name: 'admin',
      // lazy-loaded
      component: () => import('./views/BoardAdmin.vue')
    },
    {
      path: '/mod',
      name: 'moderator',
      // lazy-loaded
      component: () => import('./views/BoardModerator.vue')
    },
    {
      path: '/user',
      name: 'user',
      // lazy-loaded
      component: () => import('./views/BoardUser.vue')
    }
  ]
});

// router.beforeEach((to, from, next) => {
//   const publicPages = ['/login', '/register', '/home'];
//   const authRequired = !publicPages.includes(to.path);
//   const loggedIn = localStorage.getItem('user');

//   // trying to access a restricted page + not logged in
//   // redirect to login page
//   if (authRequired && !loggedIn) {
//     next('/login');
//   } else {
//     next();
//   }
// });
